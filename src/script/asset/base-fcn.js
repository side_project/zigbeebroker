"use strict";

// 檢查環境是否為 Web
var isWeb = true;
try {
  if (process) isWeb = false;
} catch (error) { }


var self = {
  // 取得時間字串
  getDateFormat: function (format = "YYYY-MM-DD HH:mm:ss") {
    return moment().format(format);
  },
  // 取得 Unix 時間戳記
  getUnix: function (time = 0) {
    return Math.floor(new Date() / 1000) + time;
  },
  // 格式化 Unix 時間戳記
  formatUnix: function (unix, format = "YYYY-MM-DD HH:mm:ss") {
    return moment(unix).format(format);
  },
  // 差值法
  volmap: function (x, in_min, in_max, out_min, out_max) {
    if (x >= in_max)
      x = in_max;
    if (x <= in_min)
      x = in_min;
    return parseFloat((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min);
  },
  // 保留浮點數字，去除所有文字
  parseFloat2: function (str) {
    let Length = str.length;
    let reStr = "";

    for (let i = 0; i < Length; i++) {
      if (str.charCodeAt(i) == 46
        || (str.charCodeAt(i) >= 48 && str.charCodeAt(i) <= 57)) {
        reStr += str.charAt(i);
      }
    }
    return parseFloat(reStr)
  },
  // 在左邊補 0
  padLeft: function (str, lenght) {
    str += "";

    if (str.length >= lenght)
      return str;
    else
      return self.padLeft("0" + str, lenght);
  },
  // 隨機取值
  random: function (Max) {
    return Math.random() * Max;
  },
  // 隨機取字串
  randomString: function (length) {
    let Str = "";
    for (let i = 0; i < length; i++) {
      Str += String.fromCharCode(randomChar());
    }
    return Str;
  },
  // 延遲一段時間
  delay: function (time = 0) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve();
      }, time)
    })
  },

  // 節流
  throttle: function (fn, wait) {
    let callback = fn;
    let timerId = null;

    // 是否是第一次执行
    let firstInvoke = true;

    function throttled() {
      let context = this;
      let args = arguments;

      // 如果是第一次触发，直接执行
      if (firstInvoke) {
        callback.apply(context, args);
        firstInvoke = false;
        return;
      }

      // 如果定时器已存在，直接返回。        
      if (timerId) {
        return;
      }

      timerId = setTimeout(function () {
        // 注意这里 将 clearTimeout 放到 内部来执行了
        clearTimeout(timerId);
        timerId = null;

        callback.apply(context, args);
      }, wait);
    }

    // 返回一个闭包
    return throttled;
  },
  // 去彈跳
  debounce: function (func, delay = 1000) {
    var timer = null;
    return function () {
      var context = this;
      var args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        func.apply(context, args)
      }, delay);
    }
  },

  // deep key 可以用字串表示，ex : obj.data.val = objDataByStrKey(obj, "data.val")
  objDataByStrKey: function (obj, strKey) {
    strKey = strKey.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
    strKey = strKey.replace(/^\./, '');           // strip a leading dot
    let splitKey = strKey.split('.');

    for (let i = 0, n = splitKey.length; i < n; ++i) {
      let k = splitKey[i];
      if (k in obj)
        obj = obj[k];
      else
        return
    }
    return obj;
  },

  // 物件 key-val 轉為矩陣 [{key, val}]
  obj2Array: function (obj = {}) {
    return Object.keys(obj).map((key) => {
      return {
        key,
        val: obj[key]
      }
    })
  },



  // 判斷物件是否為空
  isEmpty: function (obj) {
    if (obj == null) return true;

    if (obj.length > 0) return false;
    if (obj.length === 0) return true;

    if (typeof obj !== "object") return true;
    for (var key in obj) {
      if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
  },

  // 運行環境是否為網頁
  isWeb: function () {
    return isWeb
  },

  // 運行環境是否為 Electron
  isElectron: function () {
    return !isWeb
  },
}

export default self

// ----------------- fcn -----------------

// 隨機取字元
function randomChar() {
  let Char = parseInt(self.random(255));
  while (!isChar(Char)) {
    Char = parseInt(self.random(255));
  }
  return Char;
} // randomChar

// 是否為 a~z 0~9
function isChar(Num) {
  let Re = false;
  if (Num >= 48 && Num <= 57) {
    Re = true;
  } else if (Num >= 97 && Num <= 122) {
    Re = true;
  }

  return Re;
} // isChar




// --- Array 擴充 ---

// 隨機取出某一個元素
Array.prototype.randomItem = function () {
  let i = parseInt(self.random(this.length));
  return self.copyObj(this[i])
};

// 矩陣迭帶
Array.prototype.each = function (fn) {
  fn = fn || Function.K;
  var a = [];
  var args = Array.prototype.slice.call(arguments, 1);
  for (var i = 0; i < this.length; i++) {
    var res = fn.apply(this, [this[i], i].concat(args));
    if (res != null) a.push(res);
  }
  return a;
};

// 矩陣是否包含指定元素
Array.prototype.contains = function (suArr) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] === suArr)
      return true;
  }
  return false;
}

// 不重複元素的矩陣
Array.prototype.uniquelize = function () {
  var ra = new Array();
  for (var i = 0; i < this.length; i++) {
    if (!ra.contains(this[i])) {
      ra.push(this[i]);
    }
  }
  return ra;
};

// 交集
Array.intersect = function (a, b) {
  return a.uniquelize().each(function (o) { return b.contains(o) ? o : null });
};

// 差集
Array.minus = function (a, b) {
  return a.uniquelize().each(function (o) { return b.contains(o) ? null : o });
};

// 補集
Array.complement = function (a, b) {
  return Array.minus(Array.union(a, b), Array.intersect(a, b));
};

// 聯集
Array.union = function (a, b) {
  return a.concat(b).uniquelize();
};


// --- String 擴充 ---

String.prototype.isInteger = function (str) {
  let re = /^-?\d+$/
  return re.test(str)
}

String.prototype.isNumber = function (str) {
  let re = /(^[\-0-9][0-9]*(.[0-9]+)?)$/
  return re.test(str)
}



// --- Object 擴充 ---

const isObj = x => typeof x === "object"
// 交集
Object.intersect = function (a, b) {
  let result = {}

  if (([a, b]).every(isObj)) {
    Object.keys(a).forEach((key) => {
      const value = a[key]
      const other = b[key]

      if (isObj(value)) {
        result[key] = Object.intersect(value, other)
      } else if (value === other) {
        result[key] = value
      }
    })
  }

  return result
}

// 空物件
Object.isEmpty = function (obj) {
  if (obj == null) return true;

  if (obj.length > 0) return false;
  if (obj.length === 0) return true;

  if (typeof obj !== "object") return true;
  for (var key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }

  return true;
}

// 複製物件
Object.copy = function (obj) {
  return JSON.parse(JSON.stringify(obj))
}