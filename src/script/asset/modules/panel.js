/**
 * 參數面板，應顯示數值
 */

export default {
  zigbee: {
    "E180-ZG120A": {
      "1.0.0": {
        allParams: {
          name: "全參數",
          description: "顯示所有參數",
          params: [       // 顯示參數，對應 params.js
            "deviceType", "panId", "netStatus"
            , "coorShortAddr", "coorMacAddr"
            , "localShortAddr", "localMacAddr", "targetShortAddr", "targetMacAddr"
            , "channel", "sendMode", "outputMode"
            , "power", "bps", "localGroupNum"
            , "targetGroupNum", "wakeCycle"
            , "netOpeningHours", "rejoinCycle", "rejoinNum"
            , "remoteHeader"

            , "floor", "room"
          ],
          setters: [       // 設定框，對應 cmds.js
            "setSettingMode", "setTxMode"
            , "getDeviceAllParams", "getDeviceType"
            , "setDeviceType", "setSendMode", "setTargetMac"
            , "getPanId", "setPanId"

            , "setRoomNum", "getRoomNum"

            , "reboot"
          ],
        },

        coordinator: {
          name: "協調器",
          description: "Zigbee 協調器",
          params: [       // 顯示參數，對應 params.js
            "deviceType", "panId", "netStatus"
            , "targetShortAddr", "targetMacAddr"
            , "channel", "sendMode", "outputMode"
            , "power", "localGroupNum"
            , "targetGroupNum", "netOpeningHours", "childCount"

            , "adcPin", "adcVal"

            , "floor", "room"
          ],
          setters: [       // 設定框，對應 cmds.js
            "setSettingMode", "setTxMode"
            , "getDeviceAllParams"
            , "setDeviceType", "setSendMode", "setTargetMac"

            , "getChildCount", "remoteSetGpio", "remoteReadAdc"

            , "setQrReaderCoor", "getRoomNum", "setRoomNum"

            , "reboot"
          ],
        },
        router: {
          name: "路由器",
          description: "Zigbee 路由器",
          params: [       // 顯示參數，對應 params.js
            "deviceType", "panId", "localShortAddr"
            , "localMacAddr", "targetShortAddr", "targetMacAddr"
            , "channel", "sendMode", "outputMode"
            , "power", "bps", "localGroupNum"
            , "targetGroupNum", "netOpeningHours", "remoteHeader"
            , "rejoinCycle", "rejoinNum"

            , "floor", "room"
          ],
          setters: [       // 設定框，對應 cmds.js
            "setSettingMode", "setTxMode"
            , "getDeviceAllParams"

            , "getRoomNum", "setRoomNum"

            , "reboot"
          ],
        },
        endDevice: {
          name: "終端",
          description: "Zigbee 終端",
          params: [       // 顯示參數，對應 params.js
            "deviceType", "panId", "netStatus"
            , "coorShortAddr", "coorMacAddr"
            , "localShortAddr", "localMacAddr"
            , "channel", "outputMode"
            , "power", "localGroupNum"
            , "rejoinCycle", "rejoinNum"

            , "floor", "room"
          ],
          setters: [       // 設定框，對應 cmds.js
            "setSettingMode", "setTxMode"
            , "setDeviceType"
            , "getDeviceAllParams"

            , "setGpio"

            , "getRoomNum", "setRoomNum"

            , "reboot"
          ],
        },
        sleepEndDevice: {
          name: "休眠終端",
          description: "Zigbee 終端",
          params: [       // 顯示參數，對應 params.js
            "deviceType", "panId", "netStatus"
            , "coorShortAddr", "coorMacAddr"
            , "localShortAddr", "localMacAddr"
            , "channel", "outputMode"
            , "power", "localGroupNum"
            , "rejoinCycle", "rejoinNum"

            , "wakeCycle"

            , "floor", "room"
          ],
          setters: [       // 設定框，對應 cmds.js
            "sedSetSettingMode", "sedSetTxMode"
            , "sedSetWakeCycle", "sedSetDeviceType"
            , "sedGetDeviceAllParams"

            , "sedSetGpio"

            , "setDoorCtrler", "sedGetRoomNum", "sedSetRoomNum"

            , "sedReboot"
          ],
        },
      },
    },
  },
}