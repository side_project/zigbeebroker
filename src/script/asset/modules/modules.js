import baseFcn from "../base-fcn.js"

import modulesParams from "./params.js"
import modulesCmds from "./cmds.js"
import modulesPanel from "./panel.js"

// 定義所有模組參數、命令集


// 模組參數種類
var params = modulesParams

// 模組命令集
var cmds = modulesCmds

// 模組參數面板集
var panel = modulesPanel


var data = {
  modules: { // 所有模組清單
    /*
    種類: {
      型號: [ 版本 ]
    }
    */
  },
}


init();
var self = {
  // 取得模組清單
  getModules: function (type = "", id = "") {
    // 沒有指定種類、型號和版本
    if (type === "" && id === "")
      return data.modules

    // 只有指定種類
    if (id === "") {
      if (!data.modules[type])
        return {}
      else
        return data.modules[type]
    }

    if (!data.modules[type][id])
      return {}
    else
      return data.modules[type][id]
  },

  // 取得模組清單
  getModuleParams: function (type = "", id = "", ver = "") {
    if (type === "" || id === "" || ver === "") {
      console.error(`[ getModuleParams ] 缺少參數`);
      return {}
    }

    if (params[type]
      && params[type][id]
      && params[type][id][ver])
      return params[type][id][ver]

    return {}
  },
  // 取得模組清單
  getModuleCmds: function (type = "", id = "", ver = "") {
    if (type === "" || id === "" || ver === "") {
      console.error(`[ getModuleCmds ] 缺少參數`);
      return {}
    }

    if (cmds[type]
      && cmds[type][id]
      && cmds[type][id][ver])
      return cmds[type][id][ver]

    return {}
  },

  getPanel: function (type = "", id = "", ver = "") {
    if (!type || !id || !ver)
      return {}

    if (!panel[type] || !panel[type][id] || !panel[type][id][ver])
      return {}

    return panel[type][id][ver]
  },
}


// 初始化
function init() {
  clacModulesList()
}

// 計算定義檔所有模組清單
function clacModulesList() {
  // 簡化物件
  let cmdsModules = {}, paramsModules = {}

  // cmds
  for (let type in cmds) {
    cmdsModules[type] = {}
    for (let id in cmds[type]) {
      cmdsModules[type][id] = {}
      for (let ver in cmds[type][id]) {
        cmdsModules[type][id][ver] = ""
      }
    }
  }
  /*Object.keys(cmds).forEach((type) => {
    cmdsModules[type] = {}
    Object.keys(cmds[type]).forEach((id) => {
      cmdsModules[type][id] = {}
      Object.keys(cmds[type][id]).forEach((ver) => {
        cmdsModules[type][id][ver] = ""
      })
    })
  })//*/

  // params
  Object.keys(params).forEach((type) => {
    paramsModules[type] = {}
    Object.keys(params[type]).forEach((id) => {
      paramsModules[type][id] = {}
      Object.keys(params[type][id]).forEach((ver) => {
        paramsModules[type][id][ver] = ""
      })
    })
  })

  // 取 JSON 交集
  let intersect = Object.intersect(cmdsModules, paramsModules);

  // ver 的部分改為矩陣
  Object.keys(intersect).forEach((type) => {
    Object.keys(params[type]).forEach((id) => {
      let vers = Object.keys(params[type][id])
      intersect[type][id] = vers;
    })
  })

  data.modules = intersect
  return intersect
}



export default self


