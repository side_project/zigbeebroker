
/* 
模組參數清單。

模組種類


名稱、說明、數值型態、範圍、種類
*/

export default {
  zigbee: {
    "E180-ZG120A": {
      "1.0.0": {
        deviceType: {
          name: "設備類型",
          description: "Zigbee 設備種類。更改類型後需重啟模組，設定才能生效",
          type: "Number", format: "DEC",
          list: [
            { val: 1, description: "協調器", },
            { val: 2, description: "路由器", },
            { val: 3, description: "終端（預設）", },
            { val: 4, description: "休眠終端", },
          ]
        },
        panId: {
          name: "PANID",
          description: "Zigbee 網路識別符，同網路內所有設備 PANID 需相同",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        netStatus: {
          name: "網路狀態",
          description: "目前設備網路狀態",
          type: "Number", format: "DEC",
          list: [
            { val: 0, description: "無網路", },
            { val: 1, description: "正在加入網路", },
            { val: 2, description: "成功加入網路", },
            { val: 3, description: "有網路存在但失去父節點", },
            { val: 4, description: "正在離開網路", },
          ]
        },

        coorShortAddr: {
          name: "父節點網路短地址",
          description: "Zigbee 網路中協調器短地址",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        coorMacAddr: {
          name: "父節點網路 MAC 地址",
          description: "Zigbee 網路中協調器 MAC 地址",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        localShortAddr: {
          name: "本地網路短地址",
          description: "Zigbee 設備於網路之地址，由協調器分配，協調器本身為 0x0000",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        localMacAddr: {
          name: "本地 MAC 地址",
          description: "模組 64 bit MAC 地址，不可修改",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        targetShortAddr: {
          name: "目標網路短地址",
          description: "通訊目標短地址",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },
        targetMacAddr: {
          name: "目標 MAC 地址",
          description: "定點通訊模式使用，目標模組 64 bit MAC 地址",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          min: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
        },

        channel: {
          name: "頻道",
          description: "Zigbee 物理頻率頻道",
          type: "Number", format: "HEX",
          max: 0x1A, min: 0x0B
        },
        sendMode: {
          name: "發送模式",
          description: "模組之間訊息發送模式",
          type: "Number", format: "DEC",
          list: [
            { val: 0, description: "廣播（預設）", },
            { val: 1, description: "組播", },
            { val: 2, description: "短地址點播", },
            { val: 3, description: "長地址點播", },
          ]
        },
        outputMode: {
          name: "訊息輸出模式",
          description: "模組輸出接收數據模式",
          type: "Number", format: "DEC",
          list: [
            { val: 0, description: "透傳", },
            { val: 1, description: "數據+短地址", },
            { val: 2, description: "數據+長地址", },
            { val: 3, description: "數據+RSSI", },
            { val: 4, description: "數據+短地址+RSSI", },
            { val: 5, description: "數據+長地址+RSSI", },
          ]
        },
        power: {
          name: "發射功率",
          description: "模組無線通訊發射功率",
          type: "Number", format: "DEC",
          max: 20, min: 0
        },
        bps: {
          name: "BPS",
          description: "TX/RX TTL 通訊速度",
          type: "Number", format: "DEC",
          list: [
            { val: 0x01, description: "4800", },
            { val: 0x02, description: "9600", },
            { val: 0x03, description: "14400", },
            { val: 0x04, description: "19200", },
            { val: 0x05, description: "38400", },
            { val: 0x06, description: "50000", },
            { val: 0x07, description: "57600", },
            { val: 0x08, description: "76800", },
            { val: 0x09, description: "115200 (預設)", },
            { val: 0x0A, description: "128000", },
            { val: 0x0B, description: "230400", },
            { val: 0x0C, description: "256000", },
            { val: 0x0D, description: "460800", },
          ]
        },

        localGroupNum: {
          name: "本地網路組號",
          description: "設備在此網路組號，組播通訊使用。0 表示沒有分組，為廣播",
          type: "Number", format: "HEX",
          max: 0xFF, min: 0x00
        },
        targetGroupNum: {
          name: "目標網路組號",
          description: "組播通訊之網路組號，組播通訊使用。0 表示沒有分組，為廣播",
          type: "Number", format: "HEX",
          max: 0xFF, min: 0x00
        },

        wakeCycle: {
          name: "喚醒週期",
          description: `休眠終端專用。週期越大，平均功耗越低，但是系統響應越慢。
1~60 表示 1~60 秒，61~255 表示 60+(61-60)*10~60+(255-60)*10 秒。`,
          type: "Number", format: "DEC",
          max: 255, min: 1
        },

        netOpeningHours: {
          name: "網路開放時間",
          description: "開放時間任何設備可自由加入，1~254*10 秒，255 表示永久開放",
          type: "Number", format: "DEC",
          max: 255, min: 1
        },
        rejoinCycle: {
          name: "父節點重連週期",
          description: `當父節點斷線後，重新嘗試連線週期。單位為分鐘`,
          type: "Number", format: "DEC",
          max: 255, min: 1
        },
        rejoinNum: {
          name: "父節點重連次數",
          description: `當父節點斷線後，最大重連次數。若沒有連線成功，則清除之前網路訊息，
                      重新掃描新網路，掃描週期等於重連週期。`,
          type: "Number", format: "DEC",
          max: 255, min: 1
        },
        childCount: {
          name: "節點數量",
          description: `協調器專用，目前已連線之終端數量`,
          type: "Number", format: "DEC",
          max: 255, min: 1
        },

        remoteHeader: {
          name: "無線控制 ID",
          description: "使用點播通訊時，可以指定此 ID 控制終端功能，預設值為 0xA8 0x8A，0x00 0x00 表示關閉遠端功能",
          type: "Array", format: "HEX",
          max: [0xFF, 0xFF], min: [0x00, 0x00],
        },

        ioPin: {
          name: "GPIO 腳位",
          description: "",
          type: "Number", format: "DEC",
          list: [
            { val: 0, description: "PB14", },
            { val: 1, description: "PB15", },
            { val: 2, description: "PC06", },
            { val: 3, description: "PC07", },
          ]
        },
        ioMode: {
          name: "GPIO 模式",
          description: "",
          type: "Number", format: "DEC",
          list: [
            { val: 0, description: "輸出", },
            { val: 1, description: "輸入", },
          ]
        },
        ioLevel: {
          name: "GPIO 電位",
          description: "",
          type: "Number", format: "DEC",
          list: [
            { val: 0, description: "低", },
            { val: 1, description: "高", },
            { val: 2, description: "翻轉", },
          ]
        },

        adcPin: {
          name: "ADC 類比腳位",
          description: "",
          type: "Number", format: "DEC",
          list: [
            { val: 0x00, description: "VDD 電源", },
            { val: 0x01, description: "PC08", },
            { val: 0x02, description: "PC09", },
            { val: 0x03, description: "PC10", },
            { val: 0x04, description: "PC11", },
          ]
        },
        adcVal: {
          name: "ADC 轉換數值",
          description: ``,
          type: "Array", format: "HEX",
          max: [0x0E, 0x74], min: [0x00, 0x00],
        },

        // --- custom ---

        floor: {
          name: "樓號",
          description: ``,
          type: "Number", format: "DEC",
          max: 255, min: 0
        },
        room: {
          name: "房號",
          description: ``,
          type: "Number", format: "DEC",
          max: 254, min: 0
        },
      },
    },
  },
}