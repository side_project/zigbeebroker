/* 模組命令清單

cmd、res 中 {key} 值，表示為參數、回應值，名稱對應 params key 名稱
 
*/


export default {
  zigbee: {
    "E180-ZG120A": {
      "1.0.0": {
        setSettingMode: {
          name: "進入設定模式",
          description: "設定模式才會響應設定命令",
          type: "Array", format: "HEX",
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],
          then: ["getDeviceAllParams"],
        },
        setTxMode: {
          name: "進入傳輸模式",
          description: "傳輸模式會將所有 RX 接收值視為傳輸資料發送",
          type: "Array", format: "HEX",
          cmd: [0x2F, 0x2C, 0x2B],
          res: [0x7F, 0x7C, 0x7B],
          then: [],
        },

        getDeviceType: {
          name: "取得設備類型",
          description: "取得 Zigbee 設備類型",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x01, 0xFF],
          res: [0xFB, 0x01, "{deviceType}"],
          then: [],
        },
        getNetStatus: {
          name: "取得網路狀態",
          description: "取得設備網路狀態",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x02, 0xFF],
          res: [0xFB, 0x02, "{netStatus}"],
          then: [],
        },
        getPanId: {
          name: "取得網路 PANID",
          description: "取得網路 PANID",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, "{panId}", "{panId}"],
          then: [],
        },
        getLocalMac: {
          name: "取得本地 MAC 地址",
          description: "讀取模組 MAC 位置，固定無法修改",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x08, 0x06, 0xFF],
          res: [0xFB, 0x06,
            "{localMacAddr}", "{localMacAddr}", "{localMacAddr}", "{localMacAddr}"
            , "{localMacAddr}", "{localMacAddr}", "{localMacAddr}", "{localMacAddr}"],
          then: [],
        },
        getCoorShortAddr: {
          name: "取得父節點短地址",
          description: "讀取協調器短地址",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x02, 0x07, 0xFF],
          res: [0xFB, 0x07, "{coorShortAddr}", "{coorShortAddr}"],
          then: [],
        },
        getCoorMacAddr: {
          name: "取得父節點 MAC 地址",
          description: "讀取協調器 64bit MAC",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x08, 0x08, 0xFF],
          res: [0xFB, 0x08
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"],
          then: [],
        },
        getGroupNum: {
          name: "取得網路組號",
          description: "讀取組播群組編號",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x08, 0x08, 0xFF],
          res: [0xFB, 0x08
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"],
          then: [],
        },
        getGroupNum: {
          name: "取得通訊頻道",
          description: "讀取物理頻率頻道",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x0A, 0xFF],
          res: [0xFB, 0x0A, "{channel}"],
          then: [],
        },
        getPower: {
          name: "取得發射功率",
          description: "取得發射功率",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x0B, 0xFF],
          res: [0xFB, 0x0B, "{power}"],
          then: [],
        },
        getBps: {
          name: "取得 BPS",
          description: "取得 TTL 通訊速度",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x0C, 0xFF],
          res: [0xFB, 0x0C, "{bps}"],
          then: [],
        },
        getWakeCycle: {
          name: "取得休眠時間（喚醒週期）",
          description: "取得喚醒週期",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x0D, 0xFF],
          res: [0xFB, 0x0D, "{wakeCycle}"],
          then: [],
        },
        getSendMode: {
          name: "取得資料發送方式",
          description: "",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x26, 0xFF],
          res: [0xFB, 0x26, "{sendMode}"],
          then: [],
        },

        getDeviceAllParams: {
          name: "取得設備所有參數",
          description: "一次讀取模組所有設定數值",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x2F, 0xFE, 0xFF],
          res: [0xFB, 0xFE, "{deviceType}", "{netStatus}"
            , "{panId}", "{panId}"
            , "{localShortAddr}", "{localShortAddr}"
            , "{localMacAddr}", "{localMacAddr}", "{localMacAddr}", "{localMacAddr}"
            , "{localMacAddr}", "{localMacAddr}", "{localMacAddr}", "{localMacAddr}"
            , "{coorShortAddr}", "{coorShortAddr}"
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"
            , "{localGroupNum}", "{channel}"
            , "{power}", "{bps}", "{wakeCycle}"
            , "{targetShortAddr}", "{targetShortAddr}", "{targetGroupNum}"
            , "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}"
            , "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}"
            , "{sendMode}", "{outputMode}"
            , "{netOpeningHours}", "{rejoinCycle}", "{rejoinNum}"
            , "{remoteHeader}", "{remoteHeader}"],
          then: ["getRoomNum"],
        },

        setDeviceType: {
          name: "設定設備類型",
          description: "設定 Zigbee 設備類型",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x01, "{deviceType}", 0xFF],
          res: [0xFA, 0x01],
          then: ["reboot"],
        },
        setPanId: {
          name: "設定 PAN ID",
          description: "協調器無法設定 PAN ID，要先切到終端設定，或是關閉網路後設定，最後記得重啟",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x02, 0x03, "{panId}", "{panId}", 0xFF],
          res: [0xFA, 0x03],
          then: [],
        },

        setTargetMac: {
          name: "設定目標 MAC",
          description: "點播通訊，直接指定終端 MAC",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x08, 0x25
            , "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}"
            , "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}"
            , 0xFF],
          res: [0xFA, 0x25],
          before: ["setSettingMode"],
          then: ["getDeviceAllParams"],
        },
        setSendMode: {
          name: "設定資料發送方式",
          description: "設定點播、廣播等發送模式",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x26, "{sendMode}", 0xFF],
          res: [0xFA, 0x26],
          then: ["getSendMode"],
        },

        setPower: {
          name: "設定發射功率",
          description: "設定無線發射功率",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x0B, "{power}", 0xFF],
          res: [0xFA, 0x0B],
          then: [],
        },
        setWakeCycle: {
          name: "設定休眠時間（喚醒週期）",
          description: "取得喚醒週期",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x0D, "{wakeCycle}", 0xFF],
          res: [0xFB, 0x0D],
          then: [],
        },

        reboot: {
          name: "設備重啟",
          description: "重啟設備，套用相關設定",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x00, 0x12, 0xFF],
          res: [0xFA, 0x12],
          before: ["setSettingMode"], then: [],
        },
        restoreSetting: {
          name: "回復原廠設定",
          description: "回復所有設定值，請參考說明書",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x00, 0x13, 0xFF],
          res: [0xFA, 0x13],
          then: [],
        },


        setGpio: {
          name: "GPIO 控制",
          description: "設定 GPIO 輸入/輸出與電位",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x03, 0x20, "{ioPin}", "{ioMode}", "{ioLevel}", 0xFF],
          res: [0xFA, 0x20],
          before: ["setSettingMode"], then: [],
        },

        remoteSetGpio: {
          name: "遠端 GPIO 控制",
          description: "設定終端的 GPIO 輸入/輸出與電位，只能使用點播",
          type: "Array", format: "HEX",
          cmd: [0xA8, 0x8A, 0xFD, 0x03, 0x20, "{ioPin}", "{ioMode}", "{ioLevel}", 0xFF],
          res: [0xA8, 0x8A, 0xFA, 0x20],
          before: ["setTxMode"], then: [],
        },

        remoteReadAdc: {
          name: "遠端讀取 ADC",
          description: "讀取終端 ADC，只能使用點播",
          type: "Array", format: "HEX",
          cmd: [0xA8, 0x8A, 0xFE, 0x03, 0x22, "{adcPin}", 0xFF],
          res: [0xFB, 0x22, "{adcPin}", "{adcVal}", "{adcVal}"],
          before: ["setTxMode"], then: [],
        },

        getChildCount: {
          name: "取得終端數量",
          description: "協調器命令，取得目前已連線之終端數量",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x01, 0x32, 0xFF],
          res: [0xFB, 0x32, "{childCount}"],
          then: [],
        },

        closeNet: {
          name: "關閉網路",
          description: "某些參數需先關閉 Zigbee 網路才能設定",
          type: "Array", format: "HEX",
          cmd: [0xF5, 0x01, 0x40, 0x02, 0xFF],
          res: [0xFC, 0x40, 0x00],
          then: [],
        },


        // --- 休眠終端專用 ---

        wakeup: {
          name: "喚醒命令",
          description: "發命令給休眠終端接前，需要先喚醒，才能接收命令",
          type: "Array", format: "HEX",
          cmd: [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF],
          res: [],

          before: [], // 之前發送的命令
          then: [],   // 之後發送的命令
        },

        sedSetSettingMode: {
          name: "進入設定模式",
          description: "設定模式才會響應設定命令",
          type: "Array", format: "HEX",
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],

          before: ["wakeup"], then: ["getDeviceAllParams"],
        },
        sedSetTxMode: {
          name: "進入傳輸模式",
          description: "傳輸模式會將所有 RX 接收值視為傳輸資料發送",
          type: "Array", format: "HEX",
          cmd: [0x2F, 0x2C, 0x2B],
          res: [0x7F, 0x7C, 0x7B],
          before: ["wakeup"], then: [],
        },

        sedGetPanId: {
          name: "取得網路 PANID",
          description: "取得網路 PANID",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, "{panId}", "{panId}"],
          before: ["wakeup"], then: [],
        },

        sedSetDeviceType: {
          name: "設定設備類型",
          description: "設定 Zigbee 設備類型",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x01, "{deviceType}", 0xFF],
          res: [0xFA, 0x01],
          before: ["wakeup"], then: ["reboot"],
        },

        sedGetDeviceAllParams: {
          name: "取得設備所有參數",
          description: "一次讀取模組所有設定數值",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x2F, 0xFE, 0xFF],
          res: [0xFB, 0xFE, "{deviceType}", "{netStatus}"
            , "{panId}", "{panId}"
            , "{localShortAddr}", "{localShortAddr}"
            , "{localMacAddr}", "{localMacAddr}", "{localMacAddr}", "{localMacAddr}"
            , "{localMacAddr}", "{localMacAddr}", "{localMacAddr}", "{localMacAddr}"
            , "{coorShortAddr}", "{coorShortAddr}"
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"
            , "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}", "{coorMacAddr}"
            , "{localGroupNum}", "{channel}"
            , "{power}", "{bps}", "{wakeCycle}"
            , "{targetShortAddr}", "{targetShortAddr}", "{targetGroupNum}"
            , "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}"
            , "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}", "{targetMacAddr}"
            , "{sendMode}", "{outputMode}"
            , "{netOpeningHours}", "{rejoinCycle}", "{rejoinNum}"
            , "{remoteHeader}", "{remoteHeader}"],
          before: ["wakeup"], then: [],
        },

        sedSetRoomNum: {
          name: "設定房號",
          description: "設定設備對應的房號，調整 PANID，以網路區分",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x02, 0x03, "{floor}", "{room}", 0xFF],
          res: [0xFA, 0x03],
          before: ["wakeup", "setSettingMode"],
          then: ["getRoomNum", "getPanId"],
        },
        sedGetRoomNum: {
          name: "取得房號",
          description: "取得設備房號，對應 PANID",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, "{floor}", "{room}"],
          before: ["wakeup"], then: [],
        },

        sedSetWakeCycle: {
          name: "設定休眠時間（喚醒週期）",
          description: "設定喚醒週期",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x0D, "{wakeCycle}", 0xFF],
          res: [0xFB, 0x0D],
          before: ["wakeup"], then: [],
        },

        sedReboot: {
          name: "設備重啟",
          description: "重啟設備，套用相關設定",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x00, 0x12, 0xFF],
          res: [0xFA, 0x12],
          before: ["wakeup", "setSettingMode"], then: [],
        },

        sedSetGpio: {
          name: "GPIO 控制",
          description: "設定 GPIO 輸入/輸出與電位",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x03, 0x20, "{ioPin}", "{ioMode}", "{ioLevel}", 0xFF],
          res: [0xFA, 0x20],
          before: ["wakeup"], then: [],
        },


        // --- custom ---

        setRoomNum: {
          name: "設定房號",
          description: "設定設備對應的房號，調整 PANID，以網路區分",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x02, 0x03, "{floor}", "{room}", 0xFF],
          res: [0xFA, 0x03],
          before: ["setSettingMode"],
          then: ["closeNet", "getRoomNum", "getPanId", "reboot"],
        },
        getRoomNum: {
          name: "取得房號",
          description: "取得設備房號，對應 PANID",
          type: "Array", format: "HEX",
          cmd: [0xFE, 0x02, 0x03, 0xFF],
          res: [0xFB, 0x03, "{floor}", "{room}"],
          then: [],
        },


        setQrReaderCoor: {
          name: "設定為 QRCode 讀取器之協調器",
          description: "",
          type: "Array", format: "HEX",
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],
          then: [
            "closeNet", "sqrcStep02",
            "sqrcStep03", "sqrcStep04",
            "reboot",
          ],
        },
        sqrcStep02: {
          name: "讀取器協調器步驟 02",
          description: "發射功率 20dBm",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x0B, 0x14, 0xFF],
          res: [0xFA, 0x0B],
          then: [],
        },
        sqrcStep03: {
          name: "讀取器協調器步驟 03",
          description: "設定為長地址點播模式",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x26, 0x03, 0xFF],
          res: [0xFA, 0x26],
          then: [],
        },
        sqrcStep04: {
          name: "讀取器協調器步驟 04",
          description: "設定為協調器",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x01, 0x01, 0xFF],
          res: [0xFA, 0x01],
          then: [],
        },


        setDoorCtrler: {
          name: "設定為門鎖控制器之休眠終端",
          description: "",
          type: "Array", format: "HEX",
          cmd: [0x2A, 0x2D, 0x2E],
          res: [0x7A, 0x7D, 0x7E],
          then: [
            "closeNet",
            "sdcStep01", "sdcStep02",
            "sdcStep03", "sdcStep04",
            "reboot"
          ],
          before: ["wakeup"]
        },
        sdcStep01: {
          name: "控制器終端步驟 01",
          description: "喚醒週期設為 2s",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x0D, 0x02, 0xFF],
          res: [0xFB, 0x0D],
          then: [],
        },
        sdcStep02: {
          name: "控制器終端步驟 02",
          description: "重連次數設為 255",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x30, 0xFF, 0xFF],
          res: [0xFA, 0x30],
          then: [],
        },
        sdcStep03: {
          name: "控制器終端步驟 03",
          description: "重連週期設為 1 分鐘",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x29, 0x01, 0xFF],
          res: [0xFA, 0x29],
          then: [],
        },
        sdcStep04: {
          name: "控制器終端步驟 04",
          description: "設定為休眠終端",
          type: "Array", format: "HEX",
          cmd: [0xFD, 0x01, 0x01, 0x04, 0xFF],
          res: [0xFA, 0x01],
          then: [],
        },
      },

      "1.0.1": {
      },
    },
  },


}