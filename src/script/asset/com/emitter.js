/**
 * serialport 發射器
 * 
 * 間隔指定時間發出的排程中命令
 */

import baseFcn from "../base-fcn.js"
import DataParser from "./parser.js"


var emitter = function (portObj = null, options = {}) {
  let port = portObj    // serialport 物件
  let parser = null     // serialport 解析器

  let emitter = null    // EventEmitter 物件

  let enable = options.enable || true;    // 開始發送
  let interval = options.interval || 100;  // 命令間隔
  let intervalTimer = 0;                  // 紀錄時間戳記

  let timeout = options.timeout || 2500;   // 超時
  let timeoutTimer = null;                // 超時計時器

  let pCmd = null                         // 剛發送的命令
  let queueCmds = [];                     // 等待發送的命令
  let dataParser = new DataParser()       // 命令解析器

  let module = {                          // 模組設定資料
    params: {}, cmds: {}
  }

  init();
  processCmds();

  // --- fcn ---

  function init() {
    if (baseFcn.isWeb()) return

    if (!global.EventEmitter) {
      console.error(`[ emitter ] 無法載入 EventEmitter`);
      return
    }

    EventEmitter = global.EventEmitter;
    emitter = new EventEmitter();

    setEvent()
  }

  // 處理命令發送細節
  function processCmds() {
    setInterval(() => {
      // 不須發送命令
      if (!enable || queueCmds.length === 0) {
        //console.log("[ emitter ] 不須發送命令");
        return
      }

      // port 不可用
      if (!port || !port.isOpen) {
        //console.log("[ emitter ] port 不可用");
        return
      }

      // 等待回應，不可發送命令
      if (timeoutTimer !== null) {
        //console.log("[ emitter ] 等待回應");
        return
      }

      // 偵測時間差     
      let ms = new Date() / 1
      if (Math.abs(ms - intervalTimer) >= interval || intervalTimer === 0) {
        let cmd = queueCmds.shift()
        pCmd = Object.copy(cmd)
        port.write(cmd.cmd)
        //console.log("[ emitter ] cmd write : ", cmd.cmd);

        emitter.emit("sent", cmd)

        intervalTimer = ms
      }
    }, 10)
  }


  function clearTimeoutIimer() {
    if (timeoutTimer === null) return

    clearTimeout(timeoutTimer)
    timeoutTimer = null;//*/
  }

  // 建立 emitter 事件
  function setEvent() {
    // COM 已成功送出資料
    emitter.on("sent", (cmd) => {
      // 開始超時計時
      timeoutTimer = setTimeout(() => {
        emitter.emit("err", {
          status: "timeout",
          msg: `命令「${pCmd.name}」沒有回應`,
          cmd: pCmd
        })
        clearTimeoutIimer()
      }, timeout)

      // 不需要回應，清除計時器
      if (!cmd.res || cmd.res.length === 0)
        clearTimeoutIimer()
    })



    // 回應解析成功，清除超時計時
    emitter.on("data", (data) => {
      clearTimeoutIimer()
    })
  }

  // 增加原始格式命令
  function pushQueue(cmd) {
    queueCmds.push(cmd)
  }

  // 清空命令駐列
  function clearQueue() {
    queueCmds.length = 0;
  }

  return {
    // 傳入 serialport 物件
    setPort: function (portIn, parserIn) {
      port = portIn; parser = parserIn;
    },
    // 刪除 serialport 物件
    destroyPort: function () {
      port = null; parser = null;
      emitter.removeAllListeners();
    },
    // 傳入模組參數、命令集等資料
    setModule: function (moduleIn) {
      module = Object.copy(moduleIn)
      //console.log(moduleIn);
    },


    // 增加排程命令，key 為命令名稱，依照 params.js 定義
    addCmd: function (key, params = null) {
      console.log(`[ emitter addCmd ] 增加 ${key} 命令`);

      //console.log(`[ emitter addCmd ] `, module.cmds);

      if (!module.cmds[key])
        return {
          status: "err", msg: `${key} 查無此命令`
        }

      let cmd = Object.copy(module.cmds[key])

      // 檢查命令是否正常
      if (!cmd.cmd)
        return {
          status: "err", msg: `${key} 未定義命令數值`
        }

      // 解析命令參數，取得對應 index
      let paramKeysIndexes = dataParser.parserArrayKey(cmd.cmd)

      // 不需要輸入參數
      if (Object.isEmpty(paramKeysIndexes.keys)) {
        this.beforeCmds(cmd.before)
        pushQueue({ key, ...cmd })
        this.thenCmds(cmd.then)
        return {
          status: "sus", msg: `命令 ${key} 新增成功`
        }
      }

      // 需要輸入參數，將 params 填入 cmd 中
      let res = dataParser.fillArrayParamsByKey(params, paramKeysIndexes)
      if (res.status !== "")
        return {
          status: res.status, msg: res.msg
        }

      // 填入完成後，覆蓋原本命令
      cmd.cmd = res.cmd
      this.beforeCmds(cmd.before)
      pushQueue({ key, ...cmd })
      this.thenCmds(cmd.then)

      return {
        status: "sus", msg: `命令 ${key} 新增成功`
      }
    },
    addCmds: function (cmds = []) {
      if (cmds.length === 0) return

      cmds.forEach((cmd) => {
        this.addCmd(cmd.key, cmd.params)
      })
    },
    // 處理命令前連鎖命令
    beforeCmds: function (cmds = null) {
      if (!cmds || !Array.isArray(cmds) || cmds.length === 0) return

      cmds.forEach(key => {
        console.log(`[ thenCmds ] 先前命令 ${key}`);
        this.addCmd(key)
      })
    },
    // 處理命令後連鎖命令
    thenCmds: function (cmds = null) {
      if (!cmds || !Array.isArray(cmds) || cmds.length === 0) return

      cmds.forEach(key => {
        console.log(`[ thenCmds ] 接續命令 ${key}`);
        this.addCmd(key)
      })
    },

    clearQueue: function () {
      clearQueue()
    },

    // 開始發送
    play: function () {
      enable = true
    },
    // 暫停發送
    pause: function () {
      enable = false
    },
    // 停止發送，同時清除所有排程命令
    stop: function () {
      enable = false
      queueCmds.length = 0
    },

    on: function (event, callback) {
      emitter.on(event, callback)
      return emitter
    },
    emit: function (event, arg) {
      emitter.emit(event, arg)
      return emitter
    },
  }
}

export default emitter