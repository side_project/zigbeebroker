"use strict";

import baseFcn from '../../script/asset/base-fcn.js'
import modules from '../../script/asset/modules/modules.js'


var self = {
  props: {
    // 目前面板設定
    panel: {
      type: Object,
      required: true,
      default: {
        name: "面板名稱",
        description: "面板描述",
        params: [], // 顯示參數，對應 params.js
      }
    },

    // 模組參數列表，由 params.js 定義
    moduleParams: {
      type: Object,
      required: true,
      default: {}
    },

    // 目前接收到的參數
    receiveData: {
      type: Object,
      required: true,
      default: {}
    },
  },
  data: function () {
    return {}
  },
  created: function () {
  },
  mounted: function () {
  },
  watch: {
  },
  methods: {
    // 卡片開關
    switchPort: function () {
    },

    // 計算參數控制組件
    calcParamsCtrl: function (param) {
      let res = {
        ctrl: "undefined",
      }

      let { key, value } = param;

      let paramDefine = this.moduleParams[key];

      // 參數未定義
      if (!paramDefine) {
        console.error(`[ calcParamsCtrl ] params.js 未定義 ${key}`);
        return res
      }

      // 根據 format 轉換顯示格式
      if (paramDefine.format) {
        value = this.formatVal(value, paramDefine.format)
      }

      // 存在 list，表示為 select
      if (paramDefine.list) {
        res.ctrl = "Select"
        res.list = paramDefine.list

        // 如果 value 為單一值矩陣
        if (Array.isArray(value) && value.length === 1)
          res.value = value[0]
        else
          res.value = value
        return res
      }

      if (Array.isArray(value)
        || paramDefine.type === "Array") {
        res.ctrl = "Array"
        res.value = value

        res.max = paramDefine.max
        res.min = paramDefine.min
        return res
      }

      res.ctrl = "Number"
      res.value = value
      return res
    },

    // 根據 params.js format 定義，轉換顯示方式
    formatVal: function (value, format) {
      try {
        format = format.toLowerCase()

        if (value === "NONE") return value

        switch (format) {
          case "hex": {
            if ((Array.isArray(value)))
              return value.map(val => {
                let hex = baseFcn.padLeft(val.toString(16), 2).toUpperCase()
                return `0x${hex}`
              })

            let hex = baseFcn.padLeft(value.toString(16), 2).toUpperCase()
            return `0x${hex}`
          }

          default: {
            return value
          }
        }
      } catch (err) {
        console.warn("[ formatVal ] ", err)
        return value
      }
    },

    // 處理矩陣顯示方式
    showArray: function (array = [], symbol) {
      if (Array.isArray(array))
        return array.join(symbol)
      return array
    },
  },
  computed: {
    // 各項參數
    params: function () {
      /*
      data: {
        key: "data", // 參數在 params.js key
        name: "參數名稱",
        description: "參數說明",
      }
      */

      let data = this.panel.params.map((key) => {
        let param = {
          key,                    // 參數 key
          ctrl: "undefined",      // 控制組件類型
          name: "未定義參數",
          description: "請檢查是否有遺漏參數或 key 拼寫錯誤",
          class: "err",
          value: "NONE",
          timestamp: 0
        }

        // 檢查模組參數定義不存在
        if (!this.moduleParams[key]) return param

        let { name, description } = this.moduleParams[key]

        param.name = name;
        param.description = description;

        // 數值存在
        if (this.receiveData[key]) {
          param.timestamp = this.receiveData[key].timestamp
          param.value = this.receiveData[key].val
        }

        // 計算參數控制組件
        let ctrlType = this.calcParamsCtrl(param)
        param = Object.assign(param, ctrlType)


        return param
      })

      //console.log("params : ", data);

      return data
    },
    // 組件狀態值
    status: function () {
      let status = {
        err: "",
      }

      if (!this.panel.params || this.panel.params.length === 0)
        status.err = "無顯示參數"

      return status
    },
  }
}

export default self