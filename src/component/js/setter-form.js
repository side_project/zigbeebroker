"use strict";

import baseFcn from '../../script/asset/base-fcn.js'
import modules from '../../script/asset/modules/modules.js'

import DataParser from '../../script/asset/com/parser.js'

let dataParser = new DataParser()

var self = {
  props: {
    // 目前面板設定
    panel: {
      type: Object,
      required: true,
      default: {
        name: "面板名稱",
        description: "面板描述",
        params: [], // 顯示參數，對應 params.js
      },
    },

    // 模組參數列表，由 params.js 定義
    moduleParams: {
      type: Object,
      required: true,
      default: null,
    },

    // 模組命令列表，由 cmds.js 定義
    moduleCmds: {
      type: Object,
      required: true,
      default: null,
    },

    // card 紀錄之 setter 值
    setterRecord: {
      type: Object,
      required: true,
      default: null,
    },

    // serialport 是否開啟
    isPortOpen: {
      type: Object,
      required: true,
      default: false,
    },

    // COM 路徑
    path: {
      type: Object,
      required: true,
      default: null,
    },
  },
  data: function () {
    return {
      setters: [  // setter 設定器，儲存變數
        /*
        {
          key, timestamp: 0, // 上次發送時間
          params: {
            deviceType: {
              value: "", class: "", msg: ""
            },
          },
        }
        */
      ],
      debounce: {
        checkSetter: null,
      }
    }
  },
  created: function () {
    // 生成 debounce 物件（避免 this 無法指向 Vue 問題，改為將 function 存在 data 中）
    // 驗證輸入數值是否正確
    this.debounce.checkSetter = baseFcn.debounce(() => {
      this.setters.forEach((setter, i) => {
        this.checkSetterVal(setter, this.setterList[i])
      })

      //console.log("setters    : ", this.setters[3])
      //console.log("setterList : ", this.setterList)
    }, 200)


    // 載入紀錄
    this.loadRecord()
  },
  mounted: function () {
  },
  watch: {
    setters: {
      handler: function () {
        this.debounce.checkSetter()
      },
      deep: true
    },

    // 面板變化，就要重新生成儲存參數的 setters
    panel: {
      handler: function () {
        this.setters = []; this.setters.length = 0

        if (this.status.err !== "") return

        this.setters = this.setterList.map((cmptdSetter) => {
          let { key } = cmptdSetter
          // 生成 setters 物件，用於儲存數值
          let setterData = {
            key, timestamp: 0, isMinify: true,
            params: {},  // 數值物件，可能有多個參數
          }

          // 生成對應參數的變數，以便綁定 input
          if (Object.keys(cmptdSetter.params).length !== 0) {
            Object.keys(cmptdSetter.params).forEach((key) => {
              setterData.params[key] = {
                value: "",
                class: "",
                msg: ""     // 訊息
              }
            })
          }

          return setterData
        })

        this.loadRecord();
      },
      immediate: true
    },


  },
  methods: {
    // 發送數值
    sendSetting: function (setter, computedSetter = null) {
      if (!this.isPortOpen) {
        this.$emit("msg", {
          msg: `請開啟 Port`, type: "err"
        })
        return
      }

      //console.log("[ sendSetting ] setter         : ", setter);
      //console.log("[ sendSetting ] computedSetter : ", computedSetter);

      // 不需要參數
      if (!computedSetter)
        return this.emitSetterVal(setter)

      // 檢查所有參數是否有錯誤訊息
      let hasErr = Object.keys(setter.params).some(key => {
        let param = setter.params[key]

        if (param.class === "err" || param.class === "empty") {
          //console.log("param : ", param);
          this.$emit("msg", {
            msg: `命令「${computedSetter.name}」錯誤：${param.msg}`, type: "err"
          })
          return true
        }
        return false
      })

      if (hasErr) return

      this.emitSetterVal(setter)
    },
    // 發送 setter val
    emitSetterVal: function (setter) {
      // 更新時間戳記
      setter.timestamp = new Date() / 1

      //console.log("[ sendSetting ] ", setter)
      this.$emit("send", setter)
    },

    emitSetterChange: function () {
      //console.log("[ emitSetterChange ] setters : ", this.setters)

      this.$emit("setterChange", {
        value: this.setters
      })
    },

    // 檢查 setter value 是否正確
    checkSetterVal: function (setter, computedSetter = null) {
      //console.log("[ checkSetterVal ] setter         : ", setter)
      //console.log("[ checkSetterVal ] computedSetter : ", computedSetter)

      /*
      setter: {
        key: "setDeviceType", timestamp: 0,
        params: {
          deviceType: {
            value: "", class: "", err: ""
          },
        },
      }
      computedSetter: {
        key: "getDeviceAllParams", description: "一次讀取模組所有設定數值",
        name: "取得設備所有參數", class: "",
        noParams: true,
        params: { 同 params.js }
      }
      */

      // 不需要參數
      if (!computedSetter || computedSetter.noParams) return

      // 檢查 params 所有參數
      Object.keys(setter.params).forEach(key => {
        // 取出參數內容
        let param = setter.params[key]
        let paramDefine = computedSetter.params[key]
        //console.log("param : ", param);

        if (!paramDefine) {
          param.class = "err"
          param.msg = `params.js 未定義參數 ${key}`
          return
        }

        // 數值為空
        if (param.value === "") {
          param.class = "empty"
          param.msg = `參數「${paramDefine.name}」不可為空`
          return
        }

        // 矩陣
        if (paramDefine.type.toLowerCase() === "array") {
          let { status, msg } = checkSpaceStrArray(param.value, paramDefine)
          param.class = status; param.msg = msg
          return
        }

        // select
        if (paramDefine.list) {
          let { status, msg } = checkSelect(param.value, paramDefine)
          param.class = status; param.msg = msg
          return
        }


        // number
        if (paramDefine.type.toLowerCase() === "number") {
          let { status, msg } = checkNumberStr(param.value, paramDefine)
          param.class = status; param.msg = msg
          return
        }
      })
    },

    // 計算設定器控制組件
    calcSetterCtrl: function (setter) {
      let res = {
        noParams: true,
        params: {           // 需要輸入參數
          /*
          deviceType: {       // 對應 params.js 定義
            name: "發射功率",
            description: "模組無線通訊發射功率",
            type: "Number", format: "DEC",
            max: 20, min: 0
            ctrl: "undefined",
          }
          */
        }
      }

      let { key } = setter

      let cmdsDefine = this.moduleCmds[key]

      if (!cmdsDefine) {
        console.error(`[ calcSetterCtrl ] cmds.js 未定義 ${key}`)
        return res
      }
      if (!cmdsDefine.cmd) {
        console.error(`[ calcSetterCtrl ] ${key} 未定義 cmd 參數`)
        return res
      }

      // 不需要參數
      let { keys } = dataParser.parserArrayKey(cmdsDefine.cmd)
      if (Object.keys(keys).length === 0) return res

      // 根據 key 取得 params.js 內容，並判斷所需組件
      res.noParams = false
      Object.keys(keys).forEach(key => {
        res.params[key] = {
          key,
          name: "未定義參數",
          description: "請檢查是否有遺漏參數或 key 拼寫錯誤",
          ctrl: "undefined",
        }

        // 參數未定義
        if (!this.moduleParams[key]) return

        let paramType = this.calcParamCtrl(res.params[key])

        res.params[key] = Object.assign(res.params[key], this.moduleParams[key], paramType)
      })

      return res
    },

    // 計算參數控制組件
    calcParamCtrl: function (param) {
      let res = {
        ctrl: "undefined",
      }

      let paramDefine = this.moduleParams[param.key];

      // 參數未定義
      if (!paramDefine) {
        console.error(`[ calcParamsCtrl ] params.js 未定義 ${param.key}`);
        return res
      }

      // 存在 list，表示為 select
      if (paramDefine.list) {
        res.ctrl = "Select"
        res.list = paramDefine.list
        return res
      }

      if (paramDefine.type === "Array") {
        res.ctrl = "Array"

        res.max = paramDefine.max
        res.min = paramDefine.min
        return res
      }

      res.ctrl = "Number"
      res.max = paramDefine.max
      res.min = paramDefine.min
      return res
    },

    loadRecord: function () {
      if (!this.setterRecord) {
        console.warn(`[ ${this.path} setter loadRecord ] 無紀錄`)
        return
      }

      /*if (this.setters.length !== this.setterRecord.value.length) {
        console.warn("[ setter loadRecord ] 紀錄 setter 數量不同")
        return
      }//*/

      // 根據 key 載入
      this.setters.forEach((setter, i) => {
        let key = setter.key

        // 搜尋 key
        let recordSetter = this.setterRecord.value.find(record => {
          return record.key && (record.key === key)
        })

        if (recordSetter) this.setters[i] = recordSetter
      })

    },
  },
  computed: {
    // 所有命令清單
    cmdsList: function () {
      let cmdsDefine = this.moduleCmds[key]

      if (!cmdsDefine) return []

      return Object.keys(cmdsDefine)
    },
    // 要顯示的設定器
    setterList: function () {
      return this.panel.setters.map((key) => {
        let setter = {
          key,                    // 命令 key
          name: "未定義命令",
          description: "請檢查是否有遺漏命令或 key 拼寫錯誤",
          class: ""
        }

        // 檢查模組參數定義不存在
        if (!this.moduleCmds[key]) return setter

        let { name, description } = this.moduleCmds[key]
        setter.name = name;
        setter.description = description;

        // 計算參數控制組件
        let ctrlType = this.calcSetterCtrl(setter)
        setter = Object.assign(setter, ctrlType)

        //console.log("comSetter : ", setter);

        return setter
      })
    },

    // 整個組件狀態值
    status: function () {
      let status = {
        err: "",
      }

      if (!this.panel.setters || this.panel.setters.length === 0)
        status.err = "無設定器"

      return status
    },
  },
}

// 檢查空白分隔的矩陣字串。ex: "0x10 0xAF 0x3F"
function checkSpaceStrArray(arrayStr, define) {
  let stdMax = define.max
  let stdMin = define.min
  let format = define.format || ""

  // 多個空白替換成一個空白
  arrayStr = arrayStr.replace(/\s+/g, ' ')

  let array = null

  try {
    array = arrayStr.split(" ")
  } catch (err) {
    return {
      status: "err",
      msg: `${arrayStr} 字串矩陣解析失敗，請以「空格分隔」`
    }
  }

  if (stdMin.length !== stdMax.length) {
    return {
      status: "err",
      msg: `最大最小值定義之矩陣長度不一致，請檢查 params.js 定義檔
max : ${stdMax.join(" ")}，min : ${stdMin.join(" ")}`
    }
  }

  if (array.length !== stdMax.length) {
    return {
      status: "err",
      msg: `輸入矩陣長度錯誤，請輸入 ${stdMax.length} 位，並以「空格分隔」`
    }
  }

  let status = "", msg = ""

  // 檢查每個元素
  let err = array.some((val, i) => {
    let num = null, max = null, min = null

    try {
      num = parseInt(val)
    } catch (err) {
      status = "err"; msg = `第 ${i + 1} 位數值 ${val} 解析失敗，請確認是否為數值格式`
      return true
    }

    // 非數值
    if (isNaN(num)) {
      status = "err"; msg = `第 ${i + 1} 位數值 ${val} 解析失敗，請確認是否為數值格式`
      return true
    }

    try {
      max = parseInt(stdMax[i])
    } catch (err) {
      status = "err"; msg = `最大值定義，第 ${i + 1} 位數值 ${formatVal(stdMax[i], format)} 解析失敗，請確認是否為數值格式`
      return true
    }

    try {
      min = parseInt(stdMin[i])
    } catch (err) {
      status = "err"; msg = `最小值定義，第 ${i + 1} 位數值 ${formatVal(stdMin[i], format)} 解析失敗，請確認是否為數值格式`
      return true
    }

    if (num > max) {
      status = "err"; msg = `第 ${i + 1} 位數值 ${val} 超過最大值 ${formatVal(stdMax[i], format)}`
      return true
    }

    if (num < min) {
      status = "err"; msg = `第 ${i + 1} 位數值 ${val} 低於最小值 ${formatVal(stdMin[i], format)}`
      return true
    }

    return false
  })

  return { status, msg }
}

// 檢查 Select
function checkSelect(val, define) {
  let list = define.list

  if (!Array.isArray(list)) {
    return {
      status: "err",
      msg: `定義 list 不為矩陣，請檢查 params.js 定義檔`
    }
  }

  let status = "", msg = ""

  let err = list.some((item, i) => {
    if ((typeof item.val) === "undefined") {
      status = "err"; msg = `list 定義之第 ${i + 1} 個元素沒有定義 val，請檢查 params.js 定義檔`
      return true
    }
    return false
  })
  if (err) return { status, msg }


  let hasVal = list.some(listItem => {
    return listItem.val === val
  })

  if (!hasVal) {
    return {
      status: "err", msg: `選擇設值不在 list 定義中`
    }
  }

  return { status, msg }
}

// 檢查數值字串
function checkNumberStr(valStr, define) {
  let max = define.max
  let min = define.min
  let format = define.format || ""

  let val = null

  try {
    val = parseInt(valStr)
  } catch (err) {
    return {
      status: "err",
      msg: `${valStr} 數值字串解析失敗，請檢查是否為合法數值格式`
    }
  }

  // 非數值
  if (isNaN(val)) {
    return {
      status: "err",
      msg: `${valStr} 數值字串解析失敗，請檢查是否為合法數值格式`
    }
  }

  if (val > max) {
    return {
      status: "err", msg: `${valStr} 數值超過最大值 ${formatVal(max, format)}`
    }
  }

  if (val < min) {
    return {
      status: "err", msg: `${valStr} 數值低於最小值 ${formatVal(min, format)}`
    }
  }

  return {
    status: "", msg: ""
  }
}

// 格式化顯示數值
function formatVal(val, format) {
  format = format.toLowerCase()

  switch (format) {
    case "hex": return `0x${val.toString(16).toUpperCase()}`
    default: return val
  }
}

export default self