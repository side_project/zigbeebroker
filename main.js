// Modules to control application life and create native browser window
"use strict";

const { app, BrowserWindow, globalShortcut, ipcMain } = require('electron')
const Menu = require('electron').Menu

/*const debug = require('electron-debug');
debug();//*/

let mainWindow;

function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1920, height: 1080, show: false,
    webPreferences: {
      nodeIntegration: true,
      devTools: true
    },
    allowEval: false
  })



  Menu.setApplicationMenu(null)

  mainWindow.loadFile('public/index.html');
  mainWindow.maximize();

  mainWindow.webContents.openDevTools()

  mainWindow.once('ready-to-show', () => {
    mainWindow.show()
  })

  mainWindow.on('closed', function () {
    mainWindow = null
  })

  mainWindow.webContents.on('crashed', event => {
    console.log('[ warn ] mainWindow crashed');
    mainWindow.reload();
  });

} // createWindow

process.on('uncaughtException', function (exception) {
  console.error(exception);
});


// -------------------- app --------------------

app.on('ready', createWindow)

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

app.on('uncaughtException', (err) => {
  console.log(err);
});//*/